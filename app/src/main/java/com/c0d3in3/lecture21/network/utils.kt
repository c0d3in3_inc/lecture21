package com.c0d3in3.lecture21.network

const val BASE_URL = "https://reqres.in/api/"
const val LOGIN = "login"
const val USERS = "users"
const val UNKNOWN = "unknown"
const val HTTP_200_OK = 200
const val HTTP_201_CREATED = 201
const val HTTP_400_BAD_REQUEST = 400
const val HTTP_401_UNAUTHORIZED = 401
const val HTTP_404_NOT_FOUND = 404
const val HTTP_500_INTERNAL_SERVER_ERROR = 500
const val HTTP_204_NO_CONTENT = 204
