package com.c0d3in3.lecture21.user.ui

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.c0d3in3.lecture21.R
import com.c0d3in3.lecture21.network.USERS
import com.c0d3in3.lecture21.network.`interface`.ApiCallback
import com.c0d3in3.lecture21.network.api.ApiHandler
import com.c0d3in3.lecture21.user.model.UserModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_user.*
import java.text.SimpleDateFormat
import java.util.*

class AddUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)

        init()
    }

    private fun init(){
        addButton.setOnClickListener {
            val name = nameEditText.text.toString()
            val job = jobEditText.text.toString()
            if (name.isNotEmpty() && job.isNotEmpty()) addUser(name, job)
            else Toast.makeText(this, "Please fill all the fields!", Toast.LENGTH_LONG).show()
        }
    }

    private fun userCreateNotify(user: UserModel) {
        authLayout.visibility = View.GONE
        loadingLayout.visibility = View.VISIBLE
        loadingTextView.setTextColor(Color.GREEN)
        val date = user.createdAt

        val mDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(user.createdAt.removeRange(date.lastIndex-4, date.length))
        val spf = SimpleDateFormat("HH:mm dd-MM-yyyy")
        val nDate = Date(mDate.time)
        val formattedDate = spf.format(nDate)
        loadingTextView.text = "Add success!\nName: ${user.name}\nJob: ${user.job}\nID: ${user.id}\nCreated at: $formattedDate"
    }


    private fun addUser(name: String, job : String){
        val params = mutableMapOf<String, String>()
        params["name"] = name
        params["job"] = job
        ApiHandler.postRequest(USERS, params, object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                var userModel = Gson().fromJson(response, UserModel::class.java)
                userCreateNotify(userModel)
            }

            override fun onError(response: String, code: Int) {
                Toast.makeText(this@AddUserActivity, response, Toast.LENGTH_LONG).show()
            }
        })
    }
}
