package com.c0d3in3.lecture21.main.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.c0d3in3.lecture21.R
import com.c0d3in3.lecture21.main.adapter.ResourcesAdapter
import com.c0d3in3.lecture21.main.model.ResourceModel
import com.c0d3in3.lecture21.network.UNKNOWN
import com.c0d3in3.lecture21.network.`interface`.ApiCallback
import com.c0d3in3.lecture21.network.api.ApiHandler
import com.c0d3in3.lecture21.user.ui.AddUserActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ResourcesAdapter.CustomCallback{

    private val itemsList = arrayListOf<ResourceModel.DataModel>()
    private val adapter = ResourcesAdapter(itemsList, this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    override fun openDetailInfo(id : Int){
        val intent = Intent(this, DetailedInfoActivity::class.java)
        intent.putExtra("itemId", id)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    private fun init(){

        addUserTextView.setOnClickListener {
            val intent = Intent(this, AddUserActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        resourcesRecyclerView.layoutManager = LinearLayoutManager(this)
        resourcesRecyclerView.adapter = adapter

        ApiHandler.getRequest(UNKNOWN, object : ApiCallback{
            override fun onSuccess(response: String, code: Int) {
                val data = Gson().fromJson(response, ResourceModel::class.java)
                for(item in data.data){
                    itemsList.add(item)
                    adapter.notifyItemInserted(itemsList.size-1)
                }
            }

            override fun onError(response: String, code: Int) {
                Toast.makeText(this@MainActivity, response, Toast.LENGTH_LONG).show()
            }
        })
    }
}
